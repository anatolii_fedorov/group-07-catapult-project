#include <Servo.h>

Servo springServo;
Servo latchServo;

int springServoPin = 0;
int latchServoPin = 1;
int sensorTrigPin = 16;
int sensorEchoPin = 17;

bool launching = false;
bool debugServo = false;

int springServoStartPos = 0;
int latchServoStartPos = 90;
int maxDistance = 50;

const int initLaunchAngle = 100;
int launchAngle = initLaunchAngle;

void setup() {
  Serial.begin(9600);

  // Set up ultrasonic sensor pins
  pinMode(sensorTrigPin, OUTPUT);
  pinMode(sensorEchoPin, INPUT);

  // Initialize the servos and move them into position
  springServo.attach(springServoPin, 400, 2600);
  springServo.write(springServoStartPos);

  latchServo.attach(latchServoPin, 400, 2600);
  latchServo.write(latchServoStartPos);

  delay(1000);
}

void loop() {
  // Measure distance to nearest object with the sensor
  digitalWrite(sensorTrigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(sensorTrigPin, LOW);

  int pulseWidth = pulseIn(sensorEchoPin, HIGH);
  int distance = pulseWidth * 340 / 20000;
  Serial.print("Distance: ");
  Serial.print(distance);
  Serial.println();

  if (distance <= maxDistance) {
    launching = true;
    launch();
    delay(4000);
  }

  delay(500);
}

void launch() {
  // Extend spring
  for (int i = springServoStartPos; i <= springServoStartPos + launchAngle; i++) {
    springServo.write(i);
    delay(10);
  }

  // Release arm
  latchServo.write(latchServoStartPos - 90);
  delay(100);
  springServo.write(190);
  delay(1000);

  // Move servos back into position
  for (int i = springServoStartPos + launchAngle; i >= springServoStartPos; i--) {
    springServo.write(i);
    delay(10);
  }

  delay(100);
  latchServo.write(latchServoStartPos);
}